import psycopg2
import random
import sys
import json
import time
import xml.etree.ElementTree as ET

class Generator :
  """Generates test data from specifications in an external xml
     object and writes them to a JSON file."""
   
  DB_HOST = "127.0.0.1"
  DB_USER = "postgres"
  DB_PW = ""
  SOURCE_DB = "intersection"

  types = [ { 'id': 'absent', 'prob' : 6 },  { 'id' : 'full', 'prob' : 3 },
            { 'id' : 'damaged', 'prob' : 3}, { 'id' : 'badly', 'prob' : 2 },
            { 'id' : 'other', 'prob' : 1} ]
  duration = [ { 'id': 'minutes', 'prob' : 4 },  { 'id' : 'hours', 'prob' : 4 },
               { 'id' : 'overnight', 'prob' : 2}, { 'id' : 'days', 'prob' : 1 } ]

  majorSQL = """SELECT gid, int_id, intersec5, classifi6, classifi7, 
                       longitude, latitude, objectid, geom,
                       geom <-> st_setsrid(st_makepoint(%(longitude)s,%(latitude)s),4326) as distance
                FROM intersection2d
                WHERE classifi6 = 'MJRSL' or classifi6 = 'MJRML'
                ORDER BY distance
                LIMIT 1;"""
  closestSQL = """SELECT gid, int_id, intersec5, classifi6, classifi7, 
                         longitude, latitude, objectid, geom,
                         geom <-> st_setsrid(st_makepoint(%(longitude)s,%(latitude)s),4326) as distance
                  FROM intersection2d
                  ORDER BY distance 
                  LIMIT 1;"""

  def __init__ (self, source) :
     """Reads the source xml document and constructs a probability
        table for each setting."""
     print ('opening {0}'.format (source))
     self.doc = ET.parse (source)
     self.typesIdx = self.selectFrom (Generator.types)
     self.durationIdx = self.selectFrom (Generator.duration)

  def selectFrom (self, source) :
     """Constructs a list of keyword indices based on the probability of each one
        occuring."""
     result = []
     for index, spec in enumerate (source) :
        for i in range (spec ['prob']) :
           result.append (index)
     return result

  def generate (self, sink) :
     """Generates the output JSON."""
     with open (sink, 'w') as f :
        with self.openDatabase () as dbc :
           data = self.makeData (dbc)
           json.dump (data, f, indent = 4, separators = (', ', ' : '))

  def openDatabase (self) :
     resource = psycopg2.connect ("dbname='{0}' host='{1}' user = '{2}' password = '{3}'".
                                  format (Generator.SOURCE_DB, Generator.DB_HOST, 
                                          Generator.DB_USER, Generator.DB_PW))
     return resource.cursor ()

  def makeData (self, db) :
     """Translate the document entries into output """
     result = []
     
     for elem in self.doc.getroot () :
        place = self.getPlace (elem) 
        print (place)
        result.append (self.makeRequest (place, db))
        if 'duplicates_attrib' in place :
           for dup in range (self.countOf (place)) :
              print ('duplicating...')
              result.append (self.makeRequest (self.offset (place), db))
     return result

  def getPlace (self, elem) :
     """Translates the xml elements defining the place into a python
        dictionary structure, including the attributes definitions for
        duplicating entries."""
     result = {}
     for sse in elem :
        if sse.tag != 'duplicates' :
           result [sse.tag] = sse.text
        else: 
           name = sse.tag + '_attrib'
           result [name] = sse.attrib
     return result

  def makeRequest (self, place, cursor) :
     """Makes a single request structure to output as a single
        JSON entry. This method assigns all of the fields."""
     result = { }
     result ['location'] = self.makeLocation (place)
     result ['problem_type'] = self.select (Generator.types, self.typesIdx)
     result ['duration'] = self.select (Generator.duration, self.durationIdx)
     result ['time'] = self.makeTime ()
     if 'picture' in place :
        result ['picture'] = place ['picture']
     else :
        result ['picture'] = ''
     result ['closest'] = self.makeLocation (self.locate (place, Generator.closestSQL, cursor))
     result ['major'] = self.makeLocation (self.locate (result ['closest'], Generator.majorSQL, cursor))
     
     return result

  def countOf (self, place) :
     """Gets the number of duplicate (offset) values for the selected
     entry."""
     attr = place ['duplicates_attrib']
     max = min = 0
     if 'max' in attr :
        max = int (attr ['max'])
        if 'min' in attr :
           min = int (attr ['min'])
     return random.randint (min, max)

  def offset (self, place) :
     """Offsets the location of a duplicated entry."""
     result = { }
     coords = [ 'longitude', 'latitude' ]
     attr = place ['duplicates_attrib']
     if 'distance' in attr :
        distance = float (attr ['distance'])
     else :
        distance = 20.0
     for c in coords :
        ofs = random.uniform (-distance, distance) / 1.111E5
        result [c] = float (place [c]) + ofs
     return result

  def locate (self, transaction, sql, cursor) :
     """Locates an intersection using the nearest location lookup function in
        the posgresql geospatial database containing the intersection data. Takes
        a preset location specifier and a postgresql cursor linking to the database"""

     result = { }  
     cursor.execute (sql, transaction)

     row = cursor.fetchone ()
     for item in zip (cursor.description, row):
        name = item [0] [0]
        value = item [1]
        result [name] = value
     return result

  def makeLocation (self, src) :
     """Copies the coordinates for a location from a source dictionary."""
     loc = { }
     for n in ['longitude', 'latitude'] :
        loc [n] = float (src [n])
     return loc

     
  def makeTime (self) :
     """Makes the temporal (date/time) specification"""
     diff = random.uniform (1800, 86400)
     # select one of these returns and comment the other out
     # this returns a time value
     #>>> return time.localtime (time.time () - diff)
     # this returns an ISO format local time value string  
     t = time.localtime (time.time () - diff)
     return time.strftime ('%Y-%m-%dT%H:%M:%S', t)

  def select (self, source, indices) :
     """Selects a given keyword according to the given probability of the 
        keyword occuring."""
     descriptor = source [random.choice (indices)]
     return descriptor ['id']

try :
  gen = Generator ('coords_002.xml')
  gen.generate ('challenge.json')
except psycopg2.Warning as warning:
  if warning.pgerror != None: 
     print (warning.pgerror)
  else :
     try :
        et = errorcodes.lookup(warning.pgcode)
        print (et)
     except KeyError as secondaryError:
        print ("unknown warning in postgres link")
except psycopg2.Error as error:
  if error.pgerror != None: 
     print (error.pgerror)
  else :
     try :
        et = errorcodes.lookup(warning.pgcode)
        print (et)
     except KeyError as secondaryError:
        print ("unknown error in postgres link")

