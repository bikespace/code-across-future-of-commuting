# code-across-future-of-commuting

A project to explore and design a dashboard structure to describe demand for bicycle parking. This one day hackathon project will challenge participants to design and implement a dashboard to display the contents of a crowd-sourced database.

## Mandatories

Before starting the project, you need to download and install nodejs on your machines.

You can found it [this](https://nodejs.org/en/).

## How to start

```javascript
npm install
npm run watch
```

Click [here]('http://localhost:8080') to open the application.

Enjoy and good luck !


## Project structure

```python
static
│   README.md        # File that you reading now
│   index.html       # Main HTML file    
│   package.json     # You can add your new plugin in this file
│   rollup.config.js # Configuration Rollup
└───static
    │   css
    │    └ dashboard.css # You can add your css in this file
    └───js
         └ dashboard.js # Javascript file for the dashboard   
```
