import Dashboard from './dashboard';
import BarChart from './barChart';
import CalendarHeatmap from './calendar_heatmap';

new Dashboard();
new BarChart();
new CalendarHeatmap();
