
export default class BarChart {
    constructor() {
        var freqData = [
            { State: 'AL', freq: { low: 4786, mid: 1319, high: 249 } }
            , { State: 'AZ', freq: { low: 1101, mid: 412, high: 674 } }
            , { State: 'CT', freq: { low: 932, mid: 2149, high: 418 } }
            , { State: 'DE', freq: { low: 832, mid: 1152, high: 1862 } }
            , { State: 'FL', freq: { low: 4481, mid: 3304, high: 948 } }
            , { State: 'GA', freq: { low: 1619, mid: 167, high: 1063 } }
            , { State: 'IA', freq: { low: 1819, mid: 247, high: 1203 } }
            , { State: 'IL', freq: { low: 4498, mid: 3852, high: 942 } }
            , { State: 'IN', freq: { low: 797, mid: 1849, high: 1534 } }
            , { State: 'KS', freq: { low: 162, mid: 379, high: 471 } }
        ];

        var svg = d3.select("#bar"),
            margin = { top: 20, right: 20, bottom: 30, left: 40 },
            width = +svg.attr("width") - margin.left - margin.right,
            height = +svg.attr("height") - margin.top - margin.bottom;

        var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
            y = d3.scaleLinear().rangeRound([height, 0]);

        var g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        d3.csv("/static/data/data.csv").then(function (data) {
            console.log(data);

            data.forEach(d => {
                d.frequency = +d.frequency;
            });
            x.domain(data.map(function (d) { return d.letter; }));
            y.domain([0, d3.max(data, function (d) { return d.frequency; })]);

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            g.append("g")
                .attr("class", "axis axis--y")
                .call(d3.axisLeft(y).ticks(10, "%"))
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("text-anchor", "end")
                .text("Frequency");

            g.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) { return x(d.letter); })
                .attr("y", function (d) { return y(d.frequency); })
                .attr("width", x.bandwidth())
                .attr("height", function (d) { return height - y(d.frequency); });
        });


    }

}

