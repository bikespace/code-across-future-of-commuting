const contents = {
  ISSUES: 'ISSUES',
  PICTURE: 'PICTURE',
  MAP: 'MAP',
  HAPPENING: 'HAPPENING',
  SUMMARY: 'SUMMARY',
}

export default contents;