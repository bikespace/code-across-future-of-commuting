
const TOKEN = 'pk.eyJ1IjoidGVzc2FsdCIsImEiOiJjajU0ZGk4OTQwZDlxMzNvYWgwZmY4ZjJ2In0.zhNa8fmnHmA0d9WKY1aTjg';

import questions from './survey-questions';

export default class Dashboard {

    constructor() {
        console.log('Start dashboard ...')
        mapboxgl.accessToken = TOKEN
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [-79.402, 43.663],
            zoom: 12
        });
        this.questions = questions;
        fetch(`http://localhost:8080/static/data/challenge.json`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(data => {
                data.forEach(element => {
                    console.log(element);
                    var el = document.createElement('div');
                    el.className = 'marker';
                    var questions = this.questions;
                    console.log(questions)
                    var problems = '<div class="options"><li><em>' + questions[0].questions[0].values.find(entry => entry.key === element.problem_type).text + '</em></li></div>'

                    var date = element.time ? new Date(element.time).toLocaleString('en-US', { month: 'long', day: 'numeric' }) : '';
                    var clock = element.time ? new Date(element.time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) : '';
                    var html = '<div class="popup"><h2>Problems</h2>\
                                <div id="problems">'+
                        problems + '\
                                </div>\
                                <div class="linebreak"></div>\
                                <h2>Date and time</h2>\
                                <div class="options third12">\
                                    <li><em id="date">'+ date + '</em>  <em>&nbsp;at&nbsp;</em>  <em id="clock">' + clock + '</em></li>\
                                </div>\
                                <div class="linebreak"></div>'

                    if (element.picture !== "") {
                        html = html + '<img id="imagePopup" src="/static/images/' + element.picture + '">'
                    }
                    // make a marker for each feature and add to the map
                    new mapboxgl.Marker(el)
                        .setLngLat([element.closest.longitude, element.closest.latitude])
                        .setPopup(new mapboxgl.Popup({ offset: 25 })
                            .setHTML(html))
                        .addTo(map);
                });
            });

        });

    }
}