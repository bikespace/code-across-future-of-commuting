(function () {
'use strict';

function __$styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

const contents = {
  ISSUES: 'ISSUES',
  PICTURE: 'PICTURE',
  MAP: 'MAP',
  HAPPENING: 'HAPPENING',
  SUMMARY: 'SUMMARY',
};

const questions = [
  {
    questions: [
      {
        key: 'problem_type',
        type: contents.ISSUES,
        heading: 'What was the issue?',
        text: 'Choose what applies',
        required: true,
        error: 'Choose at least one option',
        values: [
          {
            key: 'absent',
            text: "Couldn't find bike parking"
          },
          {
            key: 'full',
            text: "Nearby bike parking is full"
          },
          {
            key: 'damaged',
            text: "Bike parking is damaged"
          },
          {
            key: 'badly',
            text: "A bike is badly parked"
          },
          {
            key: 'other',
            text: "Different problem"
          }
        ]
      }
    ]
  },
  {
    questions: [
      {
        key: 'picture',
        type: contents.PICTURE,
        heading: 'Add a photo',
        text: 'Optional',
        error: 'Picture is wrong format',
        required: false,
      }
    ]
  }, {
    questions: [
      {
        key: 'map',
        type: contents.MAP,
        heading: 'Where was the problem?',
        text: 'Pin the location',
        required: false,
      }
    ]
  }, {
    questions: [
      {
        key: 'happening',
        type: contents.HAPPENING,
        heading: 'When did this happen?',
        subtitle1: 'Date',
        subtitle2 : 'How long did you need to park?',
        error: 'Choose an option',
        required: true,
        values: [
          {
            key: 'minutes',
            text: "minutes",
            class: 'half1'
          },
          {
            key: 'hours',
            text: "hours",
            class: 'half2'
          },
          {
            key: 'overnight',
            text: "overnight",
            class: 'half1'
          },
          {
            key: 'days',
            text: "days",
            class: 'half2'
          }
        ]
      }
    ]
  }, {
    questions: [
      {
        key: 'summary',
        type: contents.SUMMARY,
        heading: 'Summary',
        required: false,
        final: true

      }
    ]
  },
];

const TOKEN = 'pk.eyJ1IjoidGVzc2FsdCIsImEiOiJjajU0ZGk4OTQwZDlxMzNvYWgwZmY4ZjJ2In0.zhNa8fmnHmA0d9WKY1aTjg';

class Dashboard {

    constructor() {
        console.log('Start dashboard ...');
        mapboxgl.accessToken = TOKEN;
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [-79.402, 43.663],
            zoom: 12
        });
        this.questions = questions;
        fetch(`http://localhost:8080/static/data/challenge.json`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(data => {
                data.forEach(element => {
                    console.log(element);
                    var el = document.createElement('div');
                    el.className = 'marker';
                    var questions$$1 = this.questions;
                    console.log(questions$$1);
                    var problems = '<div class="options"><li><em>' + questions$$1[0].questions[0].values.find(entry => entry.key === element.problem_type).text + '</em></li></div>';

                    var date = element.time ? new Date(element.time).toLocaleString('en-US', { month: 'long', day: 'numeric' }) : '';
                    var clock = element.time ? new Date(element.time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) : '';
                    var html = '<div class="popup"><h2>Problems</h2>\
                                <div id="problems">'+
                        problems + '\
                                </div>\
                                <div class="linebreak"></div>\
                                <h2>Date and time</h2>\
                                <div class="options third12">\
                                    <li><em id="date">'+ date + '</em>  <em>&nbsp;at&nbsp;</em>  <em id="clock">' + clock + '</em></li>\
                                </div>\
                                <div class="linebreak"></div>';

                    if (element.picture !== "") {
                        html = html + '<img id="imagePopup" src="/static/images/' + element.picture + '">';
                    }
                    // make a marker for each feature and add to the map
                    new mapboxgl.Marker(el)
                        .setLngLat([element.closest.longitude, element.closest.latitude])
                        .setPopup(new mapboxgl.Popup({ offset: 25 })
                            .setHTML(html))
                        .addTo(map);
                });
            });

        });

    }
}

class BarChart {
    constructor() {
        var freqData = [
            { State: 'AL', freq: { low: 4786, mid: 1319, high: 249 } }
            , { State: 'AZ', freq: { low: 1101, mid: 412, high: 674 } }
            , { State: 'CT', freq: { low: 932, mid: 2149, high: 418 } }
            , { State: 'DE', freq: { low: 832, mid: 1152, high: 1862 } }
            , { State: 'FL', freq: { low: 4481, mid: 3304, high: 948 } }
            , { State: 'GA', freq: { low: 1619, mid: 167, high: 1063 } }
            , { State: 'IA', freq: { low: 1819, mid: 247, high: 1203 } }
            , { State: 'IL', freq: { low: 4498, mid: 3852, high: 942 } }
            , { State: 'IN', freq: { low: 797, mid: 1849, high: 1534 } }
            , { State: 'KS', freq: { low: 162, mid: 379, high: 471 } }
        ];

        var svg = d3.select("#bar"),
            margin = { top: 20, right: 20, bottom: 30, left: 40 },
            width = +svg.attr("width") - margin.left - margin.right,
            height = +svg.attr("height") - margin.top - margin.bottom;

        var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
            y = d3.scaleLinear().rangeRound([height, 0]);

        var g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        d3.csv("/static/data/data.csv").then(function (data) {
            console.log(data);

            data.forEach(d => {
                d.frequency = +d.frequency;
            });
            x.domain(data.map(function (d) { return d.letter; }));
            y.domain([0, d3.max(data, function (d) { return d.frequency; })]);

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            g.append("g")
                .attr("class", "axis axis--y")
                .call(d3.axisLeft(y).ticks(10, "%"))
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("text-anchor", "end")
                .text("Frequency");

            g.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) { return x(d.letter); })
                .attr("y", function (d) { return y(d.frequency); })
                .attr("width", x.bandwidth())
                .attr("height", function (d) { return height - y(d.frequency); });
        });


    }

}

class CalendarHeatmap {

    constructor() {

        function drawCalendar(dateData) {

            var weeksInMonth = function (month) {
                var m = d3.timeMonth.floor(month);
                return d3.timeWeeks(d3.timeWeek.floor(m), d3.timeMonth.offset(m, 1)).length;
            };

            var minDate = d3.min(dateData, function (d) { return new Date(d.day) });
            var maxDate = d3.max(dateData, function (d) { return new Date(d.day) });

            var cellMargin = 2,
                cellSize = 20;

            var day = d3.timeFormat("%w"),
                week = d3.timeFormat("%U"),
                format = d3.timeFormat("%Y-%m-%d"),
                titleFormat = d3.utcFormat("%a, %d-%b");
            var monthName = d3.timeFormat("%B"),
                months = d3.timeMonth.range(d3.timeMonth.floor(minDate), maxDate);

            var svg = d3.select("#calendar").selectAll("svg")
                .data(months)
                .enter().append("svg")
                .attr("class", "month")
                .attr("height", ((cellSize * 7) + (cellMargin * 8) + 20)) // the 20 is for the month labels
                .attr("width", function (d) {
                    var columns = weeksInMonth(d);
                    return ((cellSize * columns) + (cellMargin * (columns + 1)));
                })
                .append("g");

            svg.append("text")
                .attr("class", "month-name")
                .attr("y", (cellSize * 7) + (cellMargin * 8) + 15)
                .attr("x", function (d) {
                    var columns = weeksInMonth(d);
                    return (((cellSize * columns) + (cellMargin * (columns + 1))) / 2);
                })
                .attr("text-anchor", "middle")
                .text(function (d) { return monthName(d); });

            var rect = svg.selectAll("rect.day")
                .data(function (d, i) { return d3.timeDays(d, new Date(d.getFullYear(), d.getMonth() + 1, 1)); })
                .enter().append("rect")
                .attr("class", "day")
                .attr("width", cellSize)
                .attr("height", cellSize)
                .attr("rx", 3).attr("ry", 3) // rounded corners
                .attr("fill", '#eaeaea') // default light grey fill
                .attr("y", function (d) { return (day(d) * cellSize) + (day(d) * cellMargin) + cellMargin; })
                .attr("x", function (d) { return ((week(d) - week(new Date(d.getFullYear(), d.getMonth(), 1))) * cellSize) + ((week(d) - week(new Date(d.getFullYear(), d.getMonth(), 1))) * cellMargin) + cellMargin; })
                .on("mouseover", function (d) {
                    d3.select(this).classed('hover', true);
                })
                .on("mouseout", function (d) {
                    d3.select(this).classed('hover', false);
                })
                .datum(format);

            rect.append("title")
                .text(function (d) { return titleFormat(new Date(d)); });

            var lookup = d3.nest()
                .key(function (d) { return d.day; })
                .rollup(function (leaves) {
                    return d3.sum(leaves, function (d) { return parseInt(d.count); });
                })
                .object(dateData);

            var scale = d3.scaleLinear()
                .domain(d3.extent(dateData, function (d) { return parseInt(d.count); }))
                .range([0.4, 1]); // the interpolate used for color expects a number in the range [0,1] but i don't want the lightest part of the color scheme

            rect.filter(function (d) { return d in lookup; })
                .style("fill", function (d) { return d3.interpolatePuBu(scale(lookup[d])); })
                .select("title")
                .text(function (d) { return titleFormat(new Date(d)) + ":  " + lookup[d]; });

        }

        d3.csv("./static/data/dates.csv").then(function (data) {
            drawCalendar(data);
            console.log(data);
        });
    }
}

new Dashboard();
new BarChart();
new CalendarHeatmap();

}());
